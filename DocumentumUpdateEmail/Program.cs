﻿using System;

namespace Documentum.UpdateEmail
{
    class Program
    {
        static void Main()
        {
            var dctm = new Documentum();

            try
            {
                dctm.UpdateEmails();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                dctm.Logger.AddLogEntry(ex.ToString());
                Console.Read();
            }
        }
    }
}
