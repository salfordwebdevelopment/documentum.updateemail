﻿using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Context;
using Emc.Documentum.FS.DataModel.Core.Query;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.Services.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.IO;

namespace Documentum.UpdateEmail
{
    public class Documentum
    {
        string username, password, repository, endpoint;
        public Logger Logger;        
        private Dictionary<string, string> users;
        IServiceContext serviceContext;
        ServiceFactory serviceFactory;
        IQueryService queryService;
        IObjectService objectService;
        DirectoryEntry ldapConnection;

        public Documentum()
        {
            username = ConfigurationManager.AppSettings.Get("Username");
            password = ConfigurationManager.AppSettings.Get("Password");
            repository = ConfigurationManager.AppSettings.Get("Repository");
            endpoint = ConfigurationManager.AppSettings.Get("Endpoint");
            Logger = new Logger($@"{Environment.CurrentDirectory}\Logs", $"updated_emails_{DateTime.Now:dd-MM-yyyy}.txt");
            users = new Dictionary<string, string>();
            ldapConnection = new DirectoryEntry
            {
                Path = ConfigurationManager.AppSettings.Get("LDAPPath"),
                AuthenticationType = AuthenticationTypes.Secure
            };
            setContext();
            serviceFactory = ServiceFactory.Instance;
            queryService = ServiceFactory.Instance.GetRemoteService<IQueryService>(serviceContext, "core", endpoint);
            objectService = ServiceFactory.Instance.GetRemoteService<IObjectService>(serviceContext, "core", endpoint);
        }

        public void UpdateEmails()
        {
            Logger.AddLogEntry($"\r\nStarting updating of emails at {DateTime.Now:dd/MM/yy HH:mm:ss}\r\n");
            Console.WriteLine($"\r\nStarting updating of emails at {DateTime.Now:dd/MM/yy HH:mm:ss}\r\n");

            var query = new PassthroughQuery();
            query.QueryString = ConfigurationManager.AppSettings.Get("Query");
            query.AddRepository(repository);

            var queryEx = new QueryExecution
            {
                CacheStrategyType = CacheStrategyType.DEFAULT_CACHE_STRATEGY,
                MaxResultCount = 1000,
                MaxResultPerSource = 1000
            };
            var queryResult = queryService.Execute(query, queryEx, null);
            
            Logger.AddLogEntry($"{queryResult.DataObjects.Count} accounts to update\r\n");
            Console.WriteLine($"{queryResult.DataObjects.Count} accounts to update\r\n");

            foreach (var dataObj in queryResult.DataObjects)
            {
                var propSet = dataObj.Properties;
                var oldEnail = propSet.Get("user_address").GetValueAsString().Split('@');
                var newEmail = $"{oldEnail[0].Remove(0, 4)}{ConfigurationManager.AppSettings.Get("EmailSuffix")}".Trim();
                var adEmail = getUserObject(propSet.Get("user_name").GetValueAsString());

                Logger.AddLogEntry(
                    $"\r\nObjectId: {dataObj.Identity.GetValueAsString()}\r\n" +
                    $"User Name: {propSet.Get("user_name").GetValueAsString()}\r\n" +
                    $"Old Email Address: {propSet.Get("user_address").GetValueAsString()}\r\n" +
                    $"New Email Address: {newEmail}\r\n" +
                    $"AD Email Address: {adEmail}\r\n");
                Console.WriteLine(
                    $"ObjectId: {dataObj.Identity.GetValueAsString()}\r\n" +
                    $"User Name: {propSet.Get("user_name").GetValueAsString()}\r\n" +
                    $"Old Email Address: {propSet.Get("user_address").GetValueAsString()}\r\n" +
                    $"New Email Address: {newEmail}\r\n" +
                    $"AD Email Address: {adEmail}\r\n");

                users.Add(dataObj.Identity.GetValueAsString(), !string.IsNullOrWhiteSpace(adEmail) ? adEmail : newEmail);
            }

            if (users.Count > 0)
            {
                var objIdSet = new ObjectIdentitySet();

                foreach (var id in users.Keys)
                {
                    Qualification qualification = new Qualification($"dm_user where r_object_id = '{id}'");
                    ObjectIdentity objectIdentity = new ObjectIdentity(qualification, repository);
                    objIdSet.AddIdentity(objectIdentity);
                }

                var objs = objectService.Get(objIdSet, null);

                foreach (var obj in objs.DataObjects)
                {
                    obj.Properties.Set("user_address", users[obj.Identity.GetValueAsString()]);
                }

                objectService.Update(objs, null);
            }

            Logger.AddLogEntry($"\r\nFinished updating of emails at {DateTime.Now:dd/MM/yy HH:mm:ss}\r\n");
        }

        private string getUserObject(string username)
        {
            var search = new DirectorySearcher(ldapConnection);
            search.Filter = $"cn={username}";
            var user = search.FindOne();

            return user != null && user.Properties["altsecurityidentities"].Count > 0 ?
                user.Properties["altsecurityidentities"][0].ToString() : "";
        }

        private void setContext()
        {
            var contextFactory = ContextFactory.Instance;
            serviceContext = contextFactory.NewContext();
            var repoId = new RepositoryIdentity(repository, username, password, "");
            serviceContext.AddIdentity(repoId);
        }
    }
    public class Logger
    {
        private string filePath;

        public Logger(string path, string filename)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            this.filePath = $@"{path}\{filename}";
        }

        public void AddLogEntry(string msg, params object[] args) =>
            File.AppendAllText(filePath, string.Format(msg, args));
    }
}
